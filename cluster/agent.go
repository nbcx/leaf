package cluster

import (
	"gitee.com/nbcx/leaf/network"
	"gitee.com/nbcx/leaf/network/tcp"
)

type Agent struct {
	conn *tcp.TCPConn
}

func newAgent(conn *tcp.TCPConn) network.Agent {
	a := new(Agent)
	a.conn = conn
	return a
}

func (a *Agent) Run() {}

func (a *Agent) OnClose() {}
