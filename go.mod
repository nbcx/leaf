module gitee.com/nbcx/leaf

go 1.17

require (
	github.com/golang/protobuf v1.5.2
	github.com/gorilla/websocket v1.4.2
	google.golang.org/protobuf v1.26.0 // indirect
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
)
