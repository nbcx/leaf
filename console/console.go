package console

import (
	"math"
	"strconv"

	"gitee.com/nbcx/leaf/conf"
	"gitee.com/nbcx/leaf/network/tcp"
)

var server *tcp.TCPServer

func Init() {
	if conf.ConsolePort == 0 {
		return
	}

	server = new(tcp.TCPServer)
	server.Addr = "localhost:" + strconv.Itoa(conf.ConsolePort)
	server.MaxConnNum = int(math.MaxInt32)
	server.PendingWriteNum = 100
	server.NewAgent = newAgent

	server.Start()
}

func Destroy() {
	if server != nil {
		server.Close()
	}
}
